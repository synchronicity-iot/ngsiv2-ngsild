"use strict";

var http = require('http');
var express = require("express");
var RED = require("node-red");
var fs = require("fs");

// Load configuration and overwrite defaults with
// content from the localised verion of the file
var config = require('./config.js').config
if (fs.existsSync('./config.local.js')) {	//Load local configuration, if any
	var localConfig = require('./config.local.js').config,
		extend = require('extend');
	extend(true, config, localConfig);
}

// Create an Express app
var app = express();

// Add a simple route for static content served from 'public'
app.use("/", express.static("public"));

// Create a server
var server = http.createServer(app);

// Create the settings object - see default settings.js file for other options
var settings = {
    httpAdminRoot: "/red",
    httpNodeRoot: "/api",
    userDir: "./nodered/",
    flowFile:"server.json",
    functionGlobalContext: {
        config:config
    },
    logging: {
        console: {
            level: config.logLevel,
            metrics: false,
            audit: false
        }
    }
};



// Initialise the runtime with a server and settings
RED.init(server, settings);

// Serve the editor UI from /red
app.use(settings.httpAdminRoot, RED.httpAdmin);

// Serve the http nodes UI from /api
app.use(settings.httpNodeRoot, RED.httpNode);

server.listen(config.hosts.instancePort);

// Start the runtime
RED.start();
