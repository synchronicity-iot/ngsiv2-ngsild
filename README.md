# NGSIv2 to NGSILD Atomic Service

> NGSIv2-NGISLD atomic service provides an easy way to convert NGSIv2 to NGSILD through API-endpoints

This atomic service is composed of NodeRed and [node-red-contrib-FIWARE_official](https://github.com/FIWARE/node-red-contrib-FIWARE_official) to transform NGSIv2 to NGSI-LD through API-endpoints.  
The service sends the transformed data to a endpoint specified when the service is started.

## Install

To run the atomic service the following is required:
  - docker

To build the service:
```bash
docker build -t synchronicityiot/ngsiv2-ngsild .
```

The following environment variable can be set in the command to start the service.
- **INSTANCE_PORT**: The port that the service runs on. Do note that this is not the same as the port that is been exposed by docker, if you change this remember to change the port for docker aswell. Default is `80`.
- **NGSI_LD_ENDPOINT**: The endpoint where the transformed data is sent. Default is `http://localhost:3000/ngsi-ld/v1/`.
- **USE_X_AUTH_TOKEN**: Sets whether to use x_auth_token for request to the NGSI-LD endpoint. Available options: `false` and `true`. Default is `true`.
- **X_AUTH_TOKEN**: Set the x_auth_token set to the NGSI-LD endpoint. Default is `unset`.
- **LOG_LEVEL**: Used to set the level of logs being output to the console. The following options are available: `fatal`, `error`, `warn`, `info`, `debug`, `trace` or `off`. Default is set to `info`.

To start the service with the **NGSI_LD_ENDPOINT** set to `localhost:3000/ngsi-ld/v1/`, run the following command:
```bash
docker run -p 80:80 -e NGSI_LD_ENDPOINT=localhost:3000/ngsi-ld/v1/ --name ngsiv2tongsild synchronicityiot/ngsiv2-ngsild
```
This will make the service accessible on port `80`.

## Usage
Once the service is up and running, can the API-endpoints be accssed on `localhost/api/`.
The service provides 6 endpoints, 2 for each POST, PATCH and DELETE.
  - **POST**
    - `localhost/api/ngsiv2/entities`
    - `localhost/api/ngsiv2/entities/:entityId/attrs/`
  - **PATCH**
    - `localhost/api/ngsiv2/entities/:entityId/attrs`
    - `localhost/api/ngsiv2/entities/:entityId/attrs/:attrId`
  - **DELETE**
    - `localhost/api/ngsiv2/entities/:entityId`  
    - `localhost/api/ngsiv2/entities/:entityId/attrs/:attrId`

> Note: The above endpoints are with the default port 80.

An example of a POST with a NSGIv2 Vehicle object:
```bash
curl -X "POST" "http://localhost/api/ngsiv2/entities?options=keyValues" \
  -H 'Content-Type:application/json' \
  -d $'{
    "id": "vehicle:WasteManagement:black-box",
    "type": "Vehicle",
    "category": {
      "value": [
        "municipalServices"
      ]
    },
    "location": {
      "type": "geo:json",
      "value": {
        "type": "Point",
        "coordinates": [
          56.18786,
          10.16818
        ]
      },
      "metadata": {
        "timestamp": {
          "type": "DateTime",
          "value": "2019-01-21T06:36:34.766099026Z"
        }
      }
    },
    "name": {
      "value": "vehicle:WasteManagement:black-box"
    },
    "refDevice": {
      "type": "Text",
      "value": "urn:ngsi-ld:Device:70B3D54997587419",
      "metadata": {}
    },
    "refVehicleModel": {
      "type": "Relationship",
      "value": "vehiclemodel:econic"
    },
    "serviceProvided": {
      "value": [
        "garbageCollection",
        "wasteContainerCleaning"
      ]
    },
    "vehicleType": {
      "value": "lorry"
    }
  }'
```
This will transform the data into NSGI-LD, and send it to the endpoint set when the service was started. For example with the default endpoint: `localhost:3000/ngsi-ld/v1/entities/`.

To update the above created vehicle's location. A PATCH would look like this:
```bash
curl -X "PATCH" "http://localhost/api/ngsiv2/entities/vehicle:WasteManagement:black-box/attrs?options=keyValues" \
  -H 'Content-Type: application/json' \
  -d $'{
    "location": {
      "type": "geo:json",
      "value": {
        "type": "Point",
        "coordinates": [
          55.659821,
          12.590755
        ]
      },
      "metadata": {
        "timestamp": {
          "type": "DateTime",
          "value": "2019-01-21T08:30:00.000000000Z"
        }
      }
    }
  }'
```

And to DELETE the vehicle created above:
```bash
curl -X "DELETE" "http://localhost/api/ngsiv2/entities/vehicle:WasteManagement:black-box"
```
> Note: if the entityId doesn't contain the type, you need to provide the NGSI-LD id, e.g. *urn:ngsi-ld:vehicle:WasteManagement:black-box*

To see how the service is set up, the NodeRed can be accessed on `localhost/red/`.
