/*
	SynchroniCity | Glogal configuration file
	> Do not edit "config.js" but create instead a "config.local.js" file with only the properties you would like to change;
	> an example is available in "config.local.example.js"
	Glogal configuration file
		by Thomas Gilbert
	      from Alexandra Institute http://www.alexandra.dk
*/

var hosts = {
    instancePort: int(process.env.INSTANCE_PORT) || 80,
    nvsildendpoint: {
        endpointurl: process.env.NGSI_LD_ENDPOINT || 'http://localhost:3000/ngsi-ld/v1/',
        usexauthtoken: bool(process.env.USE_X_AUTH_TOKEN) || true,
        xauthtoken: process.env.X_AUTH_TOKEN || 'unset',
    }
};

exports.config = {
    hosts: hosts,
    //{fatal, error, warn, info, debug, trace, off}
    logLevel: process.env.LOG_LEVEL || 'info',
};

function bool(str) {
    if (str === void 0) return false;
    return str.toLowerCase() === 'true';
}

function int(str) {
    if (!str) return 0;
    return parseInt(str, 10);
}

function float(str) {
    if (!str) return 0;
    return parseFloat(str, 10);
}
