FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 80

ENV INSTANCE_PORT=80 \
    NGSI_LD_ENDPOINT=http://localhost:3000/ngsi-ld/v1/ \
    USE_X_AUTH_TOKEN=true \
    X_AUTH_TOKEN=unset \
    LOG_LEVEL=info

CMD [ "node", "server.js" ]
