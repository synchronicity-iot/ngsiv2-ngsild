"use strict";
/*
	SynchroniCity | Example of local configuration file
	> Do not edit "config.js" but create instead a "config.local.js" file with only the properties you would like to change;
	> see "config.js" for the full list of options.
*/

var hosts = {
    instancePort: 8080,
    nvsiv2endpoint: {
        endpointurl: 'http://localhost:3000/ngsi-ld/v1/'
    },
    nvsildendpoint: {
        endpointurl: 'unset'
    }
};

exports.config = {
    hosts: hosts,
    //{silent, error, warn, http, info, verbose, silly}
    logLevel: 'info',
};
